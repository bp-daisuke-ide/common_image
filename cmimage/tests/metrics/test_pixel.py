import unittest
import numpy as np
from cmimage.metrics.pixel import confusion_matrix_pixel, \
    classification_score_pixel, compare_true_pred


class TestPixel(unittest.TestCase):
    """Test class of pixel.py
    """

    def test_confusion_matrix_pixel(self):
        """test method for confusion_matrix_pixel
        """
        y_true = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        y_pred = np.array([[1, 1, 1], [0, 0, 0], [0, 0, 1]])
        labels = [0, 1]
        confusion_matrix_test = np.array([[4, 2], [1, 2]])
        confusion_matrix = confusion_matrix_pixel(y_true, y_pred, labels)
        self.assertTrue(np.all(confusion_matrix_test == confusion_matrix))

    def test_classification_score_pixel(self):
        """test method for classification_score_pixel
        """
        y_true = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        y_pred = np.array([[1, 1, 1], [0, 0, 0], [0, 0, 1]])
        precision_test, recall_test, accuracy_test \
            = 2/4, 2/3, 6/9
        f1_test = 2*precision_test*recall_test/(precision_test+recall_test)
        precision, recall, f1, accuracy \
            = classification_score_pixel(y_true, y_pred)
        self.assertEqual(precision, precision_test)
        self.assertEqual(recall, recall_test)
        self.assertEqual(f1, f1_test)
        self.assertEqual(accuracy, accuracy_test)

    def test_compare_true_pred(self):
        """test method for test_compare_true_pred
        """
        y_true = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        y_pred = np.array([[1, 1, 1], [0, 0, 0], [0, 0, 1]])
        tp = (255, 255, 255)
        fp = (0, 255, 0)
        fn = (255, 0, 0)
        tn = (0, 0, 0)
        compared_test = np.array([[tp, fp, fp], [tn, fn, tn], [tn, tn, tp]])
        compared = compare_true_pred(y_true, y_pred)
        self.assertTrue(np.all(compared_test == compared))
