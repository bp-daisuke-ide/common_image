import unittest
import os
import shutil
from cmimage.metrics.split_train_valid_test import symlink_split_files, \
    symlink_split_filenames


class TestSplitTrainValidTest(unittest.TestCase):
    """Test class of split_train_valid_test.py
    """

    def test_symlink_split_files(self):
        """test method for test_symlink_split_files
        """
        src_dir = './../datasets/src_dir'
        dst_dir = './../datasets/dst_dir_files'
        dst_dir_test = './../datasets/dst_dir_files_test'
        shutil.rmtree(dst_dir)
        symlink_split_files(src_dir, dst_dir, seed=1)
        for subdir in ['train', 'valid', 'test']:
            for subsubdir in ['x', 'y']:
                for label in ['0', '1']:
                    dst_files = os.listdir(os.path.join(dst_dir,
                                                        subdir,
                                                        subsubdir,
                                                        label))
                    dst_files_test = os.listdir(os.path.join(dst_dir_test,
                                                             subdir,
                                                             subsubdir,
                                                             label))
                    dst_files = sorted(dst_files)
                    dst_files_test = sorted(dst_files_test)
                    self.assertEqual(dst_files, dst_files_test)

    def test_symlink_split_filenames(self):
        """test method for symlink_split_filenames
        """
        src_dir = './../datasets/src_dir'
        dst_dir = './../datasets/dst_dir_filenames'
        dst_dir_test = './../datasets/dst_dir_filenames_test'
        shutil.rmtree(dst_dir)
        symlink_split_filenames(src_dir, dst_dir,
                                lambda x: x.split('__')[0], seed=1)
        for subdir in ['train', 'valid', 'test']:
            for subsubdir in ['x', 'y']:
                for label in ['0', '1']:
                    dst_files = os.listdir(os.path.join(dst_dir,
                                                        subdir,
                                                        subsubdir,
                                                        label))
                    dst_files_test = os.listdir(os.path.join(dst_dir_test,
                                                             subdir,
                                                             subsubdir,
                                                             label))
                    dst_files = sorted(dst_files)
                    dst_files_test = sorted(dst_files_test)
                    self.assertEqual(dst_files, dst_files_test)
