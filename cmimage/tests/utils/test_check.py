import unittest
import os
import numpy as np
from cmimage.utils.check import get_mask_rate, get_relative_path, \
    get_value_from_filename


class TestCheck(unittest.TestCase):
    """Test class of check.py
    """

    def test_get_mask_rate(self):
        """test method for get_mask_rate
        """
        mask = np.array([[1, 0, 1], [1, 1, 0], [1, 0, 1]], dtype=np.uint8)
        mask_rate_test = 6/9
        mask_rate = get_mask_rate(mask)
        self.assertEqual(mask_rate, mask_rate_test)

    def test_get_relative_path(self):
        """test method for get_relative_path
        """
        from_path = os.path.abspath('.')
        to_path = os.path.abspath('./../../utils/check.py')
        relative_path_test = './../../utils/check.py'
        relative_path = get_relative_path(from_path, to_path)
        self.assertEqual(relative_path, relative_path_test)

    def test_get_value_from_filename(self):
        """test method for get_value_from_filename
        """
        filename = 'image0001__x=0_y=0_size=224.png'
        param = 'size'
        value_test = '224'
        value = get_value_from_filename(filename, param)
        self.assertEqual(value, value_test)
