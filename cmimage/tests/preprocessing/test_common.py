import unittest
import numpy as np
from cmimage.preprocessing.common import concatenate_grid, concatenate_line, \
    freq_channel, compose_pixel_mask


class TestCommon(unittest.TestCase):
    """Test class of common.py
    """

    def test_concatenate_grid(self):
        """test method for concatenate_grid
        """
        image1 = np.array([[(255, 255, 255), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
        image2 = np.array([[(0, 0, 0), (0, 0, 0), (255, 255, 255)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
        image3 = np.array([[(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(255, 255, 255), (0, 0, 0), (0, 0, 0)]])
        image4 = np.array([[(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (255, 255, 255)]])
        images = [[image1, image2],
                  [image3, image4]]
        concat_image_test = np.array([[(255, 255, 255), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (255, 255, 255)],
                                      [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                                      [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                                      [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                                      [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                                      [(255, 255, 255), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (255, 255, 255)]]
                                     )
        concat_image = concatenate_grid(images)

        self.assertTrue(np.all(concat_image_test == concat_image))

    def test_concatenate_line(self):
        """test method for concatenate_line
        """
        image1 = np.array([[(255, 255, 255), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
        image2 = np.array([[(0, 0, 0), (255, 255, 255), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
        image3 = np.expand_dims(np.array([[0, 0, 255],
                                          [0, 0, 0],
                                          [0, 0, 0]], dtype=np.uint8), axis=-1)
        images = [image1, image2, image3]
        concat_image_test = np.array([[(255, 255, 255), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (255, 255, 255), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (255, 255, 255)],
                                      [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                                      [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0),
                                       (0, 0, 0), (0, 0, 0), (0, 0, 0)]
                                      ])
        concat_image = concatenate_line(images)

        self.assertTrue(np.all(concat_image_test == concat_image))

    def test_freq_channel(self):
        """test method for freq_channel
        """
        image = np.array([[(255, 255, 255), (0, 0, 0), (0, 0, 0)],
                          [(0, 0, 0), (50, 100, 150), (0, 0, 0)],
                          [(0, 0, 0), (0, 0, 0), (255, 255, 255)]])
        freq_abs1_test = np.zeros((256,), dtype=np.uint8)
        freq_abs1_test[0] = 6
        freq_abs1_test[50] = 1
        freq_abs1_test[255] = 2
        freq_abs2_test = np.zeros((256,), dtype=np.uint8)
        freq_abs2_test[0] = 6
        freq_abs2_test[100] = 1
        freq_abs2_test[255] = 2
        freq_abs3_test = np.zeros((256,), dtype=np.uint8)
        freq_abs3_test[0] = 6
        freq_abs3_test[150] = 1
        freq_abs3_test[255] = 2
        freq_norm1_test = freq_abs1_test/9
        freq_norm2_test = freq_abs2_test/9
        freq_norm3_test = freq_abs3_test/9
        freq_abs1, freq_abs2, freq_abs3 = freq_channel(image, normalize=False)
        freq_norm1, freq_norm2, freq_norm3 \
            = freq_channel(image, normalize=True)
        self.assertTrue(np.all(freq_abs1 == freq_abs1_test))
        self.assertTrue(np.all(freq_abs2 == freq_abs2_test))
        self.assertTrue(np.all(freq_abs3 == freq_abs3_test))
        self.assertTrue(np.all(freq_norm1 == freq_norm1_test))
        self.assertTrue(np.all(freq_norm2 == freq_norm2_test))
        self.assertTrue(np.all(freq_norm3 == freq_norm3_test))

    def test_compose_pixel_mask(self):
        """test method for compose_pixel_mask
        """
        image = np.array([[(255, 255, 255), (0, 0, 0), (50, 50, 50)],
                          [(0, 0, 0), (49, 100, 151), (0, 0, 0)],
                          [(150, 150, 150), (0, 0, 0), (255, 255, 255)]])
        mask = np.array([[1, 0, 1], [1, 1, 0], [1, 0, 1]], dtype=np.uint8)
        image_and_mask0_test \
            = np.array([[(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                        [(0, 0, 0), (49, 100, 151), (50, 50, 50)],
                        [(150, 150, 150), (255, 255, 255), (255, 255, 255)]])
        image_and_mask0 = compose_pixel_mask(image, mask, color_space='rgb',
                                             sort_channel=0, ascending=True)

        image_and_mask1_test \
            = np.array([[(255, 255, 255), (255, 255, 255), (150, 150, 150)],
                        [(49, 100, 151), (50, 50, 50), (0, 0, 0)],
                        [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
        image_and_mask1 = compose_pixel_mask(image, mask, color_space='rgb',
                                            sort_channel=1, ascending=False)

        self.assertTrue(np.all(image_and_mask0 == image_and_mask0_test))
        self.assertTrue(np.all(image_and_mask1 == image_and_mask1_test))
