import unittest
import numpy as np
from cmimage.preprocessing.crop import separate_grid, random_crop, \
    detect_object_color, detect_object_mask, detect_objects_mask
from cmimage.file_io.image_io import imread


class TestCrop(unittest.TestCase):
    """Test class of crop.py
    """

    def test_separate_grid(self):
        """test method for separate_grid
        """

        image1 = np.array([[(255, 255, 255), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
        image2 = np.array([[(0, 0, 0), (0, 0, 0), (255, 255, 255)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
        image3 = np.array([[(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(255, 255, 255), (0, 0, 0), (0, 0, 0)]])
        image4 = np.array([[(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                           [(0, 0, 0), (0, 0, 0), (255, 255, 255)]])
        images_test = np.array([image1, image2, image3, image4])
        crop_points_test = [(x, y)
                            for y in range(0, 4, 3)
                            for x in range(0, 4, 3)]

        image = np.array([[(255, 255, 255), (0, 0, 0), (0, 0, 0),
                           (0, 0, 0), (0, 0, 0), (255, 255, 255)],
                          [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                           (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                          [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                           (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                          [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                           (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                          [(0, 0, 0), (0, 0, 0), (0, 0, 0),
                           (0, 0, 0), (0, 0, 0), (0, 0, 0)],
                          [(255, 255, 255), (0, 0, 0), (0, 0, 0),
                           (0, 0, 0), (0, 0, 0), (255, 255, 255)]]
                         )

        images, crop_points = separate_grid(image, crop_size=3)
        self.assertTrue(np.all(images == images_test))
        self.assertEqual(crop_points, crop_points_test)

    def test_random_crop(self):
        """test method for random_crop
        """
        seed = 1
        n = 3
        crop_size = 3
        ramdom_seed = np.random.RandomState(seed)
        x_rand = ramdom_seed.randint(0, 6 - crop_size, n)
        y_rand = ramdom_seed.randint(0, 6 - crop_size, n)
        crop_points_test = list()
        for x, y in zip(x_rand, y_rand):
            crop_points_test.append((x, y))
        crop_points_test = list(set(crop_points_test))

        image = np.array([[(1, 0, 0), (2, 0, 0), (3, 0, 0),
                           (4, 0, 0), (5, 0, 0), (6, 0, 0)],
                          [(7, 0, 0), (8, 0, 0), (9, 0, 0),
                           (10, 0, 0), (11, 0, 0), (12, 0, 0)],
                          [(13, 0, 0), (14, 0, 0), (15, 0, 0),
                           (16, 0, 0), (17, 0, 0), (18, 0, 0)],
                          [(19, 0, 0), (20, 0, 0), (21, 0, 0),
                           (22, 0, 0), (23, 0, 0), (24, 0, 0)],
                          [(25, 0, 0), (26, 0, 0), (27, 0, 0),
                           (28, 0, 0), (29, 0, 0), (30, 0, 0)],
                          [(31, 0, 0), (32, 0, 0), (33, 0, 0),
                           (34, 0, 0), (35, 0, 0), (36, 0, 0)]]
                         )
        crop_images_test = np.array([image[y: y + crop_size, x: x + crop_size]
                                     for x, y in crop_points_test])

        crop_images, crop_points \
            = random_crop(image, crop_size=crop_size, n=n, seed=seed)
        self.assertTrue(np.all(crop_images == crop_images_test))
        self.assertEqual(crop_points, crop_points_test)

    def test_detect_object_color(self):
        """test method for detect_object_color
        """
        file_path \
            = './../datasets/images/0000047.png'
        image = imread(file_path)
        rect_test = (231, 92, 38, 33)
        object_image_test = image[rect_test[1]: rect_test[1]+rect_test[3],
                                  rect_test[0]: rect_test[0] + rect_test[2]]
        color_range = ((30, 0, 30), (100, 255, 100))
        object_image, rect \
            = detect_object_color(image, 'rgb', color_range,
                                  minimum_object_rate=0.0)
        self.assertEqual(rect, rect_test)
        self.assertTrue(np.all(object_image == object_image_test))

    def test_detect_object_mask(self):
        """test method for detect_object_mask
        """
        image_path \
            = './../datasets/images/0000047.png'
        mask_path \
            = './../datasets/labels/0000047.png'
        image = imread(image_path)
        mask = imread(mask_path)
        object_rect_test = (238, 100, 30, 35)
        x, y, w, h = object_rect_test
        object_image_test = image[y: y+h, x: x+w]
        object_mask_test = mask[y: y+h, x: x+w]
        object_image, object_mask, object_rect \
            = detect_object_mask(image, mask)
        self.assertEqual(object_rect, object_rect_test)
        self.assertTrue(np.all(object_image == object_image_test))
        self.assertTrue(np.all(object_mask == object_mask_test))

    def test_detect_objects_mask(self):
        """test method for detect_objects_mask
        """
        image_path \
            = './../datasets/images/0000047.png'
        mask_path \
            = './../datasets/labels/0000047.png'
        image = imread(image_path)
        mask = imread(mask_path)
        list_object_rect_test = [(246, 90, 32, 55), (228, 92, 29, 50)]
        object_images_test = list()
        objects_mask_test = list()
        for rect in list_object_rect_test:
            x, y, w, h = rect
            object_images_test.append(image[y: y+h, x: x+w])
            objects_mask_test.append(mask[y: y+h, x: x+w])
        object_images, objects_mask, list_object_rect \
            = detect_objects_mask(image, mask,
                                  minimum_object_rate=0.001, margin=10)
        self.assertEqual(list_object_rect, list_object_rect_test)
        for object_image, object_image_test \
                in zip(object_images, object_images_test):
            self.assertTrue(np.all(object_image == object_image_test))
        for object_mask, object_mask_test \
                in zip(object_images, object_images_test):
            self.assertTrue(np.all(object_mask == object_mask_test))
