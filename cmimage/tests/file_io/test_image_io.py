import unittest
from cmimage.file_io.image_io import imread, imsave


class TestImageio(unittest.TestCase):
    """Test class of image_io.py
    """

    def test_imread(self):
        """test method for imread
        """
        image_path = './../datasets/images/0000047.png'
        mask_path = './../datasets/labels/0000047.png'
        image = imread(image_path)
        mask = imread(mask_path)
        image_shape = image.shape[:2]
        mask_shape = mask.shape[:2]
        self.assertEqual(image_shape, mask_shape)
        self.assertEqual(image.shape[-1], 3)
        self.assertEqual(mask.shape[-1], 1)

    def test_imsave(self):
        """test method for imsave
        """
        pass
