
def _rescale(image, rescale=None):
    if rescale is None:
        rescale = 1
    return image*rescale
