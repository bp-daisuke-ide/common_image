import os
import re
import warnings
import numpy as np


def _check_img_format(fname, image):
    _, ext = os.path.splitext(fname)
    if ext == '.jpg':
        warnings.warn('jpg is lossy compression!', Warning)
    if (ext == '.png') and (image.shape[-1] == 4):
        warnings.warn('alpha channel is contained!', Warning)


def _check_shape(image, dims=3):
    image_cvt = image.copy()
    if dims == 3:
        if len(image.shape) == 2:
            image_cvt = np.expand_dims(image, axis=-1)
        if image.shape[-1] == 4:
            image_cvt = image[:, :, :3]
    elif dims == 2:
        if (len(image.shape) == 3) and (image.shape[-1] == 1):
            image_cvt = np.squeeze(image, axis=-1)
    return image_cvt


def get_mask_rate(mask):
    """Compute mask rate

    Parameters
    ----------
    mask : array, shape = [image.shape[0], image.shape[1]]

    Returns
    -------
    mask_rate : float
        The percentage of the mask pixels in the image pixels.

    """

    mask_pixel = (mask != 0).sum()
    return mask_pixel/(mask.shape[0] * mask.shape[1])


def get_relative_path(from_path, to_path):
    """Gets relative path

    Parameters
    ----------
    from_path : str
        abspath
    to_path : str
        abspath
    Returns
    -------
    relative_path : str
        The relative_path from ``from_path`` to ``to_path``.

    """

    for char_n in range(len(from_path)):
        if from_path[:char_n+1] != to_path[:char_n+1]:
            break
    char_n = to_path[:char_n].rfind('/') + 1
    relative_path = '.'
    for _ in range(len(from_path[char_n:].split('/'))):
        relative_path = os.path.join(relative_path, '..')
    relative_path = os.path.join(relative_path, to_path[char_n:])
    return relative_path


def get_value_from_filename(filename, param, pattern='[0-9|\.]+'):
    """Get parameter value from filename string

    Parameters
    ----------
    filename : str
    param : str
        Value string of parameter
    pattern : str
        Pattern of ``value`` string

    Returns
    -------
    value : str
        Parameter value

    """

    match_text = re.findall(param+'='+pattern, filename)
    if len(match_text) != 0:
        match_text = match_text[0].rstrip('.')
    else:
        return None

    value = match_text[len(param)+1:]

    return value
