import warnings
from skimage import io
from ..utils.check import _check_shape, _check_img_format
from ..utils.convert import _rescale


def imread(filename, rescale=None):
    """Read image

    Parameters
    ----------
    filename : str
    rescale : float, optional (default=None)
        Multiply the data by the value provided.

    Returns
    -------
    image : array, shape=[image.shape[0], image.shape[1], 3 or 1(if 1-channel)]

    """

    image = io.imread(filename)
    _check_img_format(filename, image)
    image = _rescale(image, rescale)
    image = _check_shape(image, dims=3)
    return image


def imsave(filename, image, rescale=None):
    """Write image

    Parameters
    ----------
    filename : str
    image : array
    rescale : float, optional (default=None)
        Multiply the data by the value provided.

    Returns
    -------
    image : array, shape=[image.shape[0], image.shape[1], 3] or
    [image.shape[0], image.shape[1]](if 1-channel)

    """

    _check_img_format(filename, image)
    image = _rescale(image, rescale)
    image = _check_shape(image, dims=2)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        io.imsave(filename, image)
