import os
import random

from cmimage.utils.check import get_relative_path


def _mkdirs_dataset(dataset_path, labels):
    for data_type in ('train', 'valid', 'test'):
        for input_output in ('x', 'y'):
            for label in labels:
                os.makedirs(os.path.join(dataset_path,
                                         data_type, input_output, label),
                            exist_ok=True)


def _check_params(**kwargs):
    for input_output in os.listdir(kwargs['original_data_path']):
        if input_output not in ['x', 'y']:
            raise Exception()

    sum_rate = (kwargs['train_rate']+kwargs['valid_rate']+kwargs['test_rate'])
    if round(sum_rate, 2) > 1.0:
        raise ValueError('Sum of data_rate must be 1.0.')


def symlink_split_files(src_dir, dst_dir,
                        train_rate=0.7, valid_rate=0.2, test_rate=0.1,
                        is_mask=True,
                        seed=None):
    """Creates symbolic links dst pointing to src.
        This method split src files into train, valid and test,
        creates symbolic links ``dst_dir``/train, valid, test/x(, y)/labels/dst files
        pointing to ``src_dir``/x(, y)/labels/src files.

    Parameters
    ----------
    src_dir : str
        This is the source directory,
        x(, y) of sub-directory and label-names of sub-sub-directory must exist.
        x src and y src must be the same file name.
    dst_dir : str
        This is the destination directory,
    train_rate : float, optional (default=0.7)
    valid_rate : float, optional (default=0.2)
    test_rate : float, optional (default=0.1)
    is_mask : bool, optional (default=True)
        If is_mask=True, y of child directory exits.
    seed : int or 1-d array_like, optional
        Random seed used to initialize the pseudo-random number generator.


    Returns
    -------
        None

    """
    random_seed = random.Random(seed)

    _check_params(original_data_path=src_dir,
                  train_rate=train_rate,
                  valid_rate=valid_rate,
                  test_rate=test_rate)

    labels = os.listdir(os.path.join(src_dir, 'x'))

    _mkdirs_dataset(dst_dir, labels)

    base_abs_path = os.getcwd()
    for label in labels:
        filenames = os.listdir(os.path.join(src_dir, 'x', label))
        n_sample = len(filenames)
        filenames = random_seed.sample(filenames, n_sample)

        thr1 = int(n_sample * train_rate)
        thr2 = int(n_sample * round(train_rate + valid_rate, 2))

        fn_train = filenames[:thr1]
        fn_valid = filenames[thr1:thr2]
        fn_test = filenames[thr2:]
        for data_type, filenames_part in [('train', fn_train),
                                          ('valid', fn_valid),
                                          ('test', fn_test)]:
            for fn in filenames_part:
                if os.getcwd() == base_abs_path:
                    os.chdir(os.path.join(dst_dir, data_type, 'x', label))
                else:
                    os.chdir(os.path.join('./../..', 'x', label))
                symlink_dir = get_relative_path(
                    os.path.abspath(
                        os.path.join(dst_dir, data_type, 'x', label)),
                    os.path.abspath(os.path.join(src_dir, 'x', label)))
                os.symlink(os.path.join(symlink_dir, fn), fn)
                if is_mask:
                    os.chdir(os.path.join('./../..', 'y', label))
                    symlink_dir = get_relative_path(
                        os.path.abspath(
                            os.path.join(dst_dir, data_type, 'y', label)),
                        os.path.abspath(os.path.join(src_dir, 'y', label)))
                    os.symlink(os.path.join(symlink_dir, fn), fn)
            os.chdir(base_abs_path)


def symlink_split_filenames(src_dir, dst_dir,
                            get_original_filename,
                            train_rate=0.7, valid_rate=0.2, test_rate=0.1,
                            is_mask=True,
                            seed=None):
    """Creates symbolic links dst pointing to src.
        This method split src files (based on filename) into train, valid and test,
        creates symbolic links ``dst_dir``/train, valid, test/x(, y)/labels/dst files
        pointing to ``src_dir``/x(, y)/labels/src files.

    Parameters
    ----------
    src_dir : str
        This is the source directory,
        x(, y) of sub-directory and label-names of sub-sub-directory must exist.
        x src and y src must be the same file name.
    dst_dir : str
        This is the destination directory,
    get_original_filename : function
        This gets substring of filename to split files.
    train_rate : float, optional (default=0.7)
    valid_rate : float, optional (default=0.2)
    test_rate : float, optional (default=0.1)
    is_mask : bool, optional (default=True)
        If is_mask=True, y of child directory exits.
    seed : int or 1-d array_like, optional
        Random seed used to initialize the pseudo-random number generator.
    Returns
    -------
        None

    """
    random_seed = random.Random(seed)

    _check_params(original_data_path=src_dir,
                  train_rate=train_rate,
                  valid_rate=valid_rate,
                  test_rate=test_rate)

    labels = os.listdir(os.path.join(src_dir, 'x'))

    _mkdirs_dataset(dst_dir, labels)

    base_abs_path = os.getcwd()

    for label in labels:
        filenames = os.listdir(os.path.join(src_dir, 'x', label))
        filenames_header = [get_original_filename(fn) for fn in filenames]
        filenames_header = sorted(list(set(filenames_header)))
        filenames_header = random_seed.sample(filenames_header,
                                              len(filenames_header))
        thr1 = int(len(filenames_header) * train_rate)
        thr2 = int(len(filenames_header) * round(train_rate + valid_rate, 2))
        train_header = filenames_header[:thr1]
        valid_header = filenames_header[thr1:thr2]
        test_header = filenames_header[thr2:]
        fn_train = [fn for fn in filenames if
                    get_original_filename(fn) in train_header]
        fn_valid = [fn for fn in filenames if
                    get_original_filename(fn) in valid_header]
        fn_test = [fn for fn in filenames if
                   get_original_filename(fn) in test_header]

        for data_type, fn in [('train', fn_train), ('valid', fn_valid),
                              ('test', fn_test)]:
            for f in fn:
                if os.getcwd() == base_abs_path:
                    os.chdir(os.path.join(dst_dir, data_type, 'x', label))
                else:
                    os.chdir(os.path.join('./../..', 'x', label))
                symlink_dir = get_relative_path(
                    os.path.abspath(
                        os.path.join(dst_dir, data_type, 'x', label)),
                    os.path.abspath(os.path.join(src_dir, 'x', label)))
                os.symlink(os.path.join(symlink_dir, f), f)
                if is_mask:
                    os.chdir(os.path.join('./../..', 'y', label))
                    symlink_dir = get_relative_path(
                        os.path.abspath(
                            os.path.join(dst_dir, data_type, 'y', label)),
                        os.path.abspath(os.path.join(src_dir, 'y', label)))
                    os.symlink(os.path.join(symlink_dir, f), f)
            os.chdir(base_abs_path)
