import numpy as np
from sklearn.metrics import accuracy_score, f1_score, precision_score, \
    recall_score, \
    confusion_matrix
from ..utils.check import _check_shape


def _check_true_and_pred_shape(y_true, y_pred):
    if y_true.shape != y_pred.shape:
        raise ValueError('y_true.shape and y_pred.shape must be same.')


def confusion_matrix_pixel(y_true, y_pred, labels=None):
    """Compute confusion matrix to evaluate the accuracy of a classification


    Parameters
    ----------
    y_true : array, shape = [image.shape[0], image.shaep[1](, 1)]
        Ground truth image.
    y_pred : array, shape = [image.shape[0], image.shaep[1](, 1)]
        Predicted image.
    labels : array, shape = [n_classes], optional
        List of labels to index the matrix. This may be used to reorder
         or select a subset of labels.
         If none is given, those that appear at least once
         in ``y_true`` or ``y_pred`` are used in sorted order.

    Returns
    -------
    C : array, shape = [n_classes, n_classes]
        Confusion matrix

    """

    _check_true_and_pred_shape(y_true, y_pred)
    if labels is None:
        labels = [0, 1]
    true_flatten = y_true.flatten()
    pred_flatten = y_pred.flatten()
    return confusion_matrix(true_flatten, pred_flatten, labels=labels)


def classification_score_pixel(y_true, y_pred):
    """Compute precision, recall, f1-score and accuracy for each class

    Parameters
    ----------
    y_true : array, shape = [image.shape[0], image.shaep[1](, 1)]
        Ground truth image.
    y_pred : array, shape = [image.shape[0], image.shaep[1](, 1)]
        Predicted image.

    Returns
    -------
    precision : float
    recall : float
    f1_score : float
    accuracy : float

    """

    _check_true_and_pred_shape(y_true, y_pred)
    true_flatten = y_true.flatten()
    pred_flatten = y_pred.flatten()
    accuracy = accuracy_score(true_flatten, pred_flatten)
    f1 = f1_score(true_flatten, pred_flatten)
    precision = precision_score(true_flatten, pred_flatten)
    recall = recall_score(true_flatten, pred_flatten)
    return precision, recall, f1, accuracy


def compare_true_pred(y_true, y_pred):
    """Compare ``y_true`` with ``y_pred``

    Parameters
    ----------
    y_true : array, shape = [image.shape[0], image.shaep[1](, 1)]
        Ground truth image.
    y_pred : array, shape = [image.shape[0], image.shaep[1](, 1)]
        Predicted image.

    Returns
    -------
    compared : array, shape = [image.shape[0], image.shape[1], 3]
        True positive, False negative, False positive and True negative map.

    """

    _check_true_and_pred_shape(y_true, y_pred)
    true_img = _check_shape(y_true, dims=3)
    pred_img = _check_shape(y_pred, dims=3)
    compared = _marking_true_pred(true_img, pred_img)
    return compared


def _marking_true_pred(true_img, pred_img):
    fn = (true_img > pred_img).prod(axis=2).astype(bool)
    fp = (true_img < pred_img).prod(axis=2).astype(bool)
    tp = (((true_img == pred_img).prod(axis=2))
          & (pred_img != 0).prod(axis=2)).astype(bool)
    true_pred_img = np.zeros(true_img.shape[:-1] + (3,), dtype=np.uint8)
    true_pred_img[fn] = (255, 0, 0)
    true_pred_img[fp] = (0, 255, 0)
    true_pred_img[tp] = (255, 255, 255)
    return true_pred_img
