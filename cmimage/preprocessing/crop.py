import numpy as np
import cv2


def _check_crop_size(image, crop_size):
    if (image.shape[0] < crop_size) or (image.shape[1] < crop_size):
        raise ValueError('crop_size must be smaller than image.shape.')


def separate_grid(image, crop_size):
    """Separates image into grid images

    Parameters
    ----------
    image : array
    crop_size : int
        A grid image size

    Returns
    -------
    grid_images: array, shape = [n_grid_images, ``crop_size``, ``crop_size``, ]
    crop_points: array, shape = [n_grid_images, 2]
        The coordinates of ``grid_images``

    """
    _check_crop_size(image, crop_size)
    img_x, img_y = image.shape[1], image.shape[0]
    crop_points = [(x, y)
                   for y in range(0, img_y-crop_size+1, crop_size)
                   for x in range(0, img_x-crop_size+1, crop_size)]
    crop_images = [image[y: y+crop_size, x: x+crop_size]
                   for x, y in crop_points]

    return np.array(crop_images), crop_points


def random_crop(image, crop_size, n=100, seed=None):
    """Apply random cropping on images as data augmentation

    Parameters
    ----------
    image : array
    crop_size : int
        A crop image size
    n : int, optional (n = 100)
        The number of crop images
    seed : int or 1-d array_like, optional
        Random seed used to initialize the pseudo-random number generator.

    Returns
    -------
    crop_images: array, shape = [n, ``crop_size``, ``crop_size``, ]
    crop_points: array, shape = [n, 2]
        The coordinates of ``crop_images``

    """
    ramdom_seed = np.random.RandomState(seed)
    _check_crop_size(image, crop_size)
    img_x, img_y = image.shape[1], image.shape[0]

    crop_points = list()
    while True:
        if len(crop_points) == n:
            break
        n_sampling = n - len(crop_points)
        x_rand = ramdom_seed.randint(0, img_x - crop_size, n_sampling)
        y_rand = ramdom_seed.randint(0, img_y - crop_size, n_sampling)
        for x, y in zip(x_rand, y_rand):
            crop_points.append((x, y))
        crop_points = list(set(crop_points))
    crop_images = [image[y: y+crop_size, x: x+crop_size]
                   for x, y in crop_points]
    return np.array(crop_images), crop_points


def detect_object_color(image, color_space='rgb', color_range=None,
                        minimum_object_rate=0.0, margin=0):
    """Detects an object within the color range as a bounding rectangle image

    Parameters
    ----------
    image : array
        RGB image
    color_space : str, optional (default='rgb')
        The ``color_space`` used for objective detection.
        Supported ``color_space`` are "rgb" and "hsv".
    color_range : array, optional (default=((0, 0, 0), (255, 255, 255)))
        The ``color_range`` on the ``color_space`` for objective detection.
    minimum_object_rate : float, optional (default=0.0)
        The minimum percentage of the object pixels in the image pixels.
    margin : int, optional (default=0)
        The number of margin pixels

    Returns
    -------
    object_image : array
    rect_max : tuple, size = 4
        The up-left point, width and height of bounding rectangle
        rect = (x, y, width, height)

    """

    if color_space == 'hsv':
        image_cvt = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
    elif color_space == 'rgb':
        image_cvt = image.copy()
    else:
        raise NotImplementedError()

    if color_range is None:
        color_range = ((0, 0, 0), (255, 255, 255))
    min_values, max_values = color_range[0], color_range[1]
    mask = cv2.inRange(image_cvt, min_values, max_values)
    _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE,
                                      cv2.CHAIN_APPROX_NONE)

    area_threshold = int(image.shape[0] * image.shape[1] * minimum_object_rate)
    areas = list()
    list_rect = list()
    for contour in contours:
        areas.append(cv2.contourArea(contour))
        x, y, w, h = cv2.boundingRect(contour)
        list_rect.append((x, y, w, h))

    max_area_idx = np.argmax(areas)
    rect_max = list_rect[max_area_idx]
    if rect_max[2]*rect_max[3] >= area_threshold:
        object_image, rect_max = _add_margin(image, rect_max, margin)
    else:
        raise Exception('Object not detected.')
    return object_image, rect_max


def detect_object_mask(image, mask, margin=0):
    """Detects a bounding rectangle image in which all objects in mask appear

    Parameters
    ----------
    image : array
    mask : array, shape = [image.shape[0], image.shape[1]]
        The annotated image (binary image) for objective detection.
    margin : int, optional (default=0)
        The number of margin pixels.

    Returns
    -------
    object_image : array,
    object_mask : array,
    object_rect : tuple, size = 4
        The up-left point, width and height of bounding rectangle
        rect = (x, y, width, height)
    """

    rect = cv2.boundingRect(mask)
    object_image, object_rect = _add_margin(image, rect, margin)
    object_mask, _ = _add_margin(mask, rect, margin)
    return object_image, object_mask, object_rect


def detect_objects_mask(image, mask, minimum_object_rate=0.01, margin=0):
    """Detects the bounding rectangle images in which each objects in mask appear

    Parameters
    ----------
    image : array
    mask : array, shape = [image.shape[0], image.shape[1]]
        The annotated image (binary image) for objective detection.
    minimum_object_rate : float, optional (default=0.0)
        The minimum percentage of the object pixels in the image pixels.
    margin : int, optional (default=0)
        The number of margin pixels.

    Returns
    -------
    object_images : array, shape = [n_objects, object_image.shape]
    objects_mask : array, , shape = [n_objects, object_image.shape]
    list_object_rect : array, shape = [n_objects, 4]
        The up-left point, width and height of bounding rectangle
        rect = (x, y, width, height)

    """

    area_threshold = int(image.shape[0] * image.shape[1] * minimum_object_rate)
    _, contours, _ = cv2.findContours(mask, cv2.RETR_TREE,
                                      cv2.CHAIN_APPROX_NONE)
    areas = list()
    list_rect = list()
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > area_threshold:
            areas.append(area)
            rect = cv2.boundingRect(contour)
            list_rect.append(rect)

    desc_area_idx = np.argsort(areas)[::-1]
    list_rect = list(np.array(list_rect)[desc_area_idx])
    object_images = list()
    objects_mask = list()
    list_object_rect = list()
    for rect in list_rect:
        object_image, object_rect = _add_margin(image, rect, margin)
        object_images.append(object_image)
        object_mask, _ = _add_margin(mask, object_rect, margin)
        objects_mask.append(object_mask)
        list_object_rect.append(object_rect)
    return object_images, objects_mask, list_object_rect


def _add_margin(image, rect, margin):
    x_img, y_img = image.shape[1], image.shape[0]
    x, y, w, h = rect
    x_max = np.clip(x+w+margin, 0, x_img)
    y_max = np.clip(y+h+margin, 0, y_img)
    x_min = np.clip(x-margin, 0, x_img)
    y_min = np.clip(y-margin, 0, y_img)
    return image[y_min: y_max, x_min: x_max], (x_min, y_min,
                                               x_max-x_min, y_max-y_min)
