import numpy as np
import cv2
from ..utils.check import _check_shape


def concatenate_grid(images):
    """Join a sequence of images along an existing axis.

    Parameters
    ----------
    images : array, shape = [n_y_images, n_x_images, image.shape[0],
    image.shape[1], image.shape[2]]
        A 2-dimensional array of images to be concatenated.

    Returns
    -------
    concat_image : array, shape = [n_y_images*image.shape[0],
    n_x_images*image.shape[1], image.shape[2]]
        The concatenated image.

    """

    concat_lines = [np.concatenate(line, axis=1) for line in images]
    concat_image = np.concatenate(concat_lines, axis=0)
    return concat_image


def concatenate_line(images, axis=1):
    """Join a sequence of images along an existing axis.

    Parameters
    ----------
    images : array, shape = [n_images, image.shape[0], image.shape[1],
    image.shape[2]]
        An array of images to be concatenated.
    axis : int, optional
        The axis along which the arrays will be joined. If axis is None,
        arrays are flattened before use. Default is 1.

    Returns
    -------
    concat_image : array
        The concatenated image.

    """

    images_rgb = [cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
                  if image.shape[-1] == 1 else image for image in images]
    concat_image = np.concatenate(images_rgb, axis=axis)
    return concat_image


def freq_channel(image, n_channles=3, normalize=True):
    """Estimate channel value distribution of the image

    Parameters
    ----------
    image : array
    n_channles : int, optional (default=3)
    normalize : bool, optional (default=True)

    Returns
    -------
    freq : array, shape = [n_channels, 256]
        An array of channel value distribution.

    """
    if image.shape[-1] < n_channles:
        n_channles = image.shape[-1]

    pixel_channel = list()
    for i in range(n_channles):
        pixel_channel.append(image[:, :, i].flatten())

    freq = list()
    for pixel in np.array(pixel_channel):
        if normalize:
            freq.append([np.sum(pixel == i)/len(pixel) for i in range(256)])
        else:
            freq.append([np.sum(pixel == i) for i in range(256)])
    return np.array(freq)


def compose_pixel_mask(image, mask, color_space='hsv', sort_channel=0,
                       ascending=True):
    """Compose image of masked pixels.

    Parameters
    ----------
    image : array
    mask : array, shape = [image.shape[0], image.shape[1], 1]
    color_space : str, optional (default='hsv')
        Color space of `sort_channel`.
    sort_channel : int, optional (default=0)
        Channel to be sorted.
    ascending : bool, optional (default=True)
        Sort ascending vs. descending.

    Returns
    -------
    image_and_mask : array
        A image composed of intersection pixels of image and mask.

    """
    mask_cvt = _check_shape(mask, dims=3)
    image_and_mask = cv2.bitwise_and(image, image, mask=mask_cvt)
    image_and_mask = image_and_mask[np.any(mask_cvt != 0, axis=-1)]
    if color_space == 'hsv':
        image_and_mask_cvt = cv2.cvtColor(np.expand_dims(image_and_mask,
                                                         axis=0),
                                          cv2.COLOR_RGB2HSV)[0]
    elif color_space == 'rgb':
        image_and_mask_cvt = image_and_mask.copy()
    else:
        raise NotImplementedError()
    n_side = int(np.ceil(np.sqrt(len(image_and_mask))))
    blank_pixel = np.zeros((n_side**2-len(image_and_mask_cvt),
                            image.shape[-1]), dtype=np.uint8)
    image_and_mask = np.array(list(image_and_mask) + list(blank_pixel))
    image_and_mask_cvt = np.array(list(image_and_mask_cvt) + list(blank_pixel))
    if ascending:
        image_and_mask \
            = image_and_mask[image_and_mask_cvt[:, sort_channel].argsort()]
    else:
        image_and_mask \
            = image_and_mask[image_and_mask_cvt[:, sort_channel].argsort()[::-1]]

    image_and_mask = image_and_mask.reshape((n_side, n_side, image.shape[-1]))

    return image_and_mask
