# Common_image

## 概要 

このリポジトリでは、画像案件で共通して利用出来そうなソースコードを管理しています。
本モジュールで扱われていない画像処理についてはOpenCVやKerasをご参照ください。

- OpenCV

    画像処理全般

    - 英語 
        http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html

    - 日本語 
        http://labs.eecs.tottori-u.ac.jp/sd/Member/oyamada/OpenCV/html/py_tutorials/py_tutorials.html

- Keras

    主にData Augumentation

    - 英語 
        http://keras.io/preprocessing/image/

    - 日本語
        http://keras.io/ja/preprocessing/image/

## 環境

本モジュールは下記ソフトウェアとそのバージョンに依存します:
- Python 3.6.1

その他、必要なPythonモジュールについては requirements.txt をご参照ください。

## インストール
Pythonライブラリは pip により管理しています。以下のコマンドで開発・実行に必要なライブラリをインストールしてください。

```
cd <path_to_the_project_directory>
pip3 install -r requirements.txt
```

また、以下のコマンドにより、本モジュールを Python が参照出来るようになります。（import 出来るようになります）
```
python3 setup.py install
```

## リポジトリ構造

リポジトリの構造は以下のようになっています。
詳細や使い方に関しては、 scripts ディレクトリにあるスクリプトと、モジュールの docstring をご参照ください。  
examples でも各モジュールの使用例や出力結果をファイルにまとめているのでご参照ください。

- cmimage

    画像案件で共通して使えそうな機能をまとめたモジュール

    - file_io:
    
        ファイル入出力に関するモジュール
     
        - imread: 画像を読み込む関数
        - imsave: 画像を保存する関数

    - metrics:
    
        モデルの性能評価に関するモジュール
        
        - confusion_matrix_pixel: 二値化画像である正解画像と予測結果画像のピクセルレベルの混同行列を返す関数
        - classification_score_pixel: 二値化画像である正解画像と予測結果画像のピクセルレベルの適合率、再現率、F値、正答率を返す関数
        - compare_true_pred: 二値化画像である正解画像と予測結果画像の比較画像を返す関数
        - symlink_split_files: 対象ディレクトリ内のファイルを train, validation, test に分割し、シンボリックリンクを作成する関数
        - symlink_split_filenames: 対象ディレクトリ内のファイルをファイル名を元に train, validation, test に分割し、シンボリックリンクを作成する関数

    - models（未実装のため、各案件のリポジトリ等を参照）:
        
        異常検知、分類、セマンティックセグメンテーション等を行うモデルのモジュール
           
    - preprocessing:
    
        元画像をモデルの入力となる特徴量へと加工する前処理をまとめたモジュール
        
        - concatenate_grid: 二次元リスト内の画像を繋ぎ合わせて一つの画像を返す関数
        - concatenate_line: リスト内の画像を繋ぎ合わせて一つの画像を返す関数
        - freq_channel: チャネルにおける画素値ごとの頻度を算出し返す関数
        - compose_pixel_mask: 元画像に対してマスクされた領域内のピクセルをまとめた画像を返す関数
        - separate_grid: 画像を格子状の小画像に分割する関数
        - random_crop: 画像から固定長の小画像を任意の枚数分、ランダムに取得する関数
        - detect_object_color: 指定した色空間内のオブジェクトを囲う、矩形画像を返す関数
        - detect_object_mask: マスク領域を全て含む矩形画像を返す関数
        - detect_objects_mask: 各マスク領域を囲う矩形画像を返す関数
        
    - utils:
        
        ユーティリティー関数のモジュール
        
        - get_mask_rate: マスク画像におけるマスクされた領域の割合を返す関数
        - get_relative_path: 2つの絶対パスの相対パスを返す関数
        - get_value_from_filename: 文字列中にある param=value のように書かれたパラメータ値を返す関数
        
    - tests:
    
        テストコード
        
        
- examples

    cmimageの各モジュールの使用例をまとめている。

    - datasets : 
    
        Objective detectionの結果, 元画像を小領域で切り出した画像、train-validation-testの分割結果
        などを出力したディレクトリ    
    
    - outputs : 
        
        アノテーションでマスクされているピクセルをまとめた画像、画像の画素値ヒストグラム、
        モデルの評価結果の例などを出力したディレクトリ
    
    - make_dataset.py :
        
        examplesで用いる元画像とアノテーション画像（二値化画像）をiccv09Dataからサンプリングし出力しているスクリプト
    
    - aggregate.py :

        アノテーションでマスクされているピクセルをまとめた画像、画像の画素値ヒストグラムを出力するスクリプト
    
    - crop_image.py :

        元画像に対してObjective detection, croppingを適用するスクリプト
    
    - split_dataset.py
    
        データをtrain, validation, testに分割しシンボリックリンクを作成するスクリプト    
        
    - evaluations.py

        アノテーション画像と予測結果画像（二値化画像）を比較し、評価結果を算出するスクリプト    

- iccv09Data
    - examplesで使用した元データ
        http://dags.stanford.edu/projects/scenedataset.html


## 免責

本リポジトリのソースコードの全ては、あくまでも開発中の分析用コードですので、自己責任でご利用ください。  
お問い合わせやバグ報告などは井出（daisuke.ide@brainpad.co.jp）までご連絡ください。
