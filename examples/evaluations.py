import os

import numpy as np
from cmimage.metrics.pixel import compare_true_pred, confusion_matrix_pixel, \
    classification_score_pixel
from cmimage.preprocessing.common import concatenate_grid, concatenate_line
from cmimage.utils.check import get_value_from_filename
from cmimage.file_io import imread, imsave


IMG_PATH = './datasets/split/image/test/x'
MASK_PATH = './datasets/split/image/test/y'
OUTPUT_PATH = './outputs/evaluations'

LABELS = ['0', '1']


def main():
    filenames = list()
    images = list()
    masks = list()
    list_pred = list()

    for label in LABELS:
        images_part = list()
        masks_part = list()
        filenames_part = os.listdir(os.path.join(IMG_PATH, label))
        filenames.extend(filenames_part)
        for fn in filenames_part:
            img = imread(os.path.join(IMG_PATH, label, fn))
            mask = imread(os.path.join(MASK_PATH, label, fn), rescale=1. / 255)
            images_part.append(img)
            masks_part.append(mask.astype(np.uint8))
        images.extend(images_part)
        masks.extend(masks_part)

        # Create a pseudo prediction
        pred_part = np.roll(masks_part, 3, axis=1)
        list_pred.extend(pred_part)
        images_part = np.array(images_part)
        masks_part = np.array(masks_part)
        pred_part = np.array(pred_part)

        # Compare y_true with y_pred
        compared_imgs = [compare_true_pred(y_true, y_pred)
                         for y_true, y_pred in zip(masks_part, pred_part)]

        # Concatenate image with compared_img
        quality_imgs = [concatenate_line([image, compared])
                        for image, compared in zip(images_part, compared_imgs)]

        quality_dir = os.path.join(OUTPUT_PATH, 'grid', 'quality', label)
        os.makedirs(quality_dir, exist_ok=True)
        for filename, quality_img in zip(filenames_part, quality_imgs):
            imsave(os.path.join(quality_dir, filename), quality_img)

        quantity_dir = os.path.join(OUTPUT_PATH, 'grid', 'quantity', label)
        os.makedirs(quantity_dir, exist_ok=True)
        with open(os.path.join(quantity_dir, 'score.tsv'), 'w') as f:
            f.write('{}\t'*8+'{}\n'
                    .format('filename', 'tp', 'fn', 'fp', 'tn',
                            'precision', 'recall', 'f1', 'accuracy'))
            for filename, mask, pred \
                    in zip(filenames_part, masks_part, pred_part):
                print(filename)
                cm = confusion_matrix_pixel(mask, pred)
                tp, fn, fp, tn = cm[1, 1], cm[1, 0], cm[0, 1], cm[1, 1]
                precision, recall, f1, accuracy \
                    = classification_score_pixel(mask, pred)
                print(cm)
                print(precision, recall, f1, accuracy)
                f.write('{}\t'*8+'{}\n'
                        .format(filename, tp, fn, fp, tn,
                                precision, recall, f1, accuracy))

    # Get original file names
    headers = list(set([filename.split('__')[0] for filename in filenames]))

    quantity_lines = list()
    for header in headers:
        header_idx = np.array([i for i, filename in enumerate(filenames)
                               if filename.startswith(header)])
        filenames_image = np.array(filenames)[header_idx]

        # Get parameter value from filename string
        crop_size = int(get_value_from_filename(filenames_image[0],
                                                param='size'))
        n_side = int(np.ceil(np.sqrt(len(filenames_image))))
        n_blank = n_side**2 - len(filenames_image)
        panel_idx = [header_idx[i: i+n_side]
                     for i in range(0, len(header_idx), n_side)]
        panel_image = list()
        panel_mask = list()
        panel_pred = list()
        for line in panel_idx:
            panel_image.append([images[idx] for idx in line])
            panel_mask.append([masks[idx] for idx in line])
            panel_pred.append([list_pred[idx] for idx in line])
        if len(panel_image) != n_side:
            panel_image.append(list())
            panel_mask.append(list())
            panel_pred.append(list())
        for _ in range(n_blank):
            panel_image[-1].append(np.zeros((crop_size, crop_size, 3),
                                            dtype=np.uint8))
            panel_mask[-1].append(np.zeros((crop_size, crop_size, 1),
                                  dtype=np.uint8))
            panel_pred[-1].append(np.zeros((crop_size, crop_size, 1),
                                  dtype=np.uint8))

        # Join a sequence of images along an existing axis.
        concat_image = concatenate_grid(panel_image)
        concat_mask = concatenate_grid(panel_mask)
        concat_pred = concatenate_grid(panel_pred)

        # Compare y_true with y_pred
        compared = compare_true_pred(concat_mask, concat_pred)

        # Concatenate image with compared_img
        quality_img = concatenate_line([concat_image, compared])

        quality_dir = os.path.join(OUTPUT_PATH, 'grid', 'quality')
        os.makedirs(quality_dir, exist_ok=True)
        imsave(os.path.join(quality_dir, header + '.png'), quality_img)

        # Compute confusion matrix to evaluate the accuracy of a classification
        cm = confusion_matrix_pixel(concat_mask, concat_pred)
        tp, fn, fp, tn = cm[1, 1], cm[1, 0], cm[0, 1], cm[1, 1]

        # Compute precision, recall, f1-score and accuracy for each class
        precision, recall, f1, accuracy \
            = classification_score_pixel(concat_mask, concat_pred)
        print(cm)
        print(precision, recall, f1, accuracy)
        quantity_lines.append((header, tp, fn, fp, tn,
                               precision, recall, f1, accuracy))

    quantity_dir = os.path.join(OUTPUT_PATH, 'grid', 'quantity')
    os.makedirs(quantity_dir, exist_ok=True)

    with open(os.path.join(quantity_dir, 'score.tsv'), 'w') as f:
        f.write('{}\t'*8+'{}\n'
                .format('filename', 'tp', 'fn', 'fp', 'tn',
                        'precision', 'recall', 'f1', 'accuracy'))
        for score in quantity_lines:
            filename, tp, fn, fp, tn, precision, recall, f1, accuracy = score
            f.write('{}\t'*8+'{}\n'
                    .format(filename, tp, fn, fp, tn,
                            precision, recall, f1, accuracy))


if __name__ == '__main__':
    main()
