import os

from cmimage.metrics.split_train_valid_test \
    import symlink_split_files, symlink_split_filenames

SRC_PATH = './datasets/separate/'
DST_PATH = './datasets/split/'


def main():
    # Creates symbolic links dst pointing to src.
    # This method split src files into train, valid and test.
    symlink_split_files(SRC_PATH,
                        os.path.join(DST_PATH, 'grid'),
                        seed=1)

    # Creates symbolic links dst pointing to src.
    # This method split src files (based on filename) into train, valid and test.
    symlink_split_filenames(SRC_PATH,
                            os.path.join(DST_PATH, 'image'),
                            get_original_filename=lambda x: x.split('__')[0],
                            seed=1)


if __name__ == '__main__':
    main()
