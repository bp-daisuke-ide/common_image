import os
import numpy as np
from cmimage.preprocessing import freq_channel, compose_pixel_mask
from cmimage.file_io import imread, imsave


IMG_PATH = './datasets/images'
MASK_PATH = './datasets/labels'
OUTPUT_PATH = './outputs/aggregates'

CROP_SIZE = 64


def main():
    filenames = os.listdir(IMG_PATH)

    os.makedirs(os.path.join(OUTPUT_PATH, 'frequency'), exist_ok=True)
    os.makedirs(os.path.join(OUTPUT_PATH, 'pixel_mask'), exist_ok=True)

    for fn in filenames:
        header, ext = os.path.splitext(fn)
        img = imread(os.path.join(IMG_PATH, fn))
        mask = imread(os.path.join(MASK_PATH, fn),
                      rescale=1. / 255).astype(np.uint8)

        # Estimate channel value distribution of the image
        hist_channel = freq_channel(img)
        with open(os.path.join(OUTPUT_PATH, 'frequency', header + '.tsv'),
                  'w') as f:
            f.write(fn + '\t')
            for hist in hist_channel:
                f.write('\t'.join([str(round(value, 3)) for value in hist]))
            f.write('\n')

        # Compose image of masked pixels
        pixel_mask = compose_pixel_mask(img, mask)
        imsave(os.path.join(OUTPUT_PATH, 'pixel_mask', fn), pixel_mask)

if __name__ == '__main__':
    main()
