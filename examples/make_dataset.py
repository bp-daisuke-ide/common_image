import os
import numpy as np
from skimage.io import imread, imsave


IMG_PATH = './../iccv09Data/images'
MASK_PATH = './../iccv09Data/labels'
OUTPUT_PATH = './../datasets'


def main():
    filenames = os.listdir(IMG_PATH)
    filenames = filenames[:5]

    for fn in filenames:
        img = imread(os.path.join(IMG_PATH, fn))

        # Convert labels.txt to mask image
        fn_label = fn.replace('.jpg', '.regions.txt')
        label = np.zeros(img.shape[:-1], dtype=np.uint8)
        with open(os.path.join(MASK_PATH, fn_label)) as f:
            for y, line in enumerate(f.readlines()):
                label[y, :] = [int(s) for s in line.rstrip('\n').split(' ')]
        label = (label == 1).astype(np.uint8)*255
        fn = fn.replace('jpg', 'png')
        imsave(os.path.join(OUTPUT_PATH, 'images', fn), img)
        imsave(os.path.join(OUTPUT_PATH, 'labels', fn), label)


if __name__ == '__main__':
    main()
