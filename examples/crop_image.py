import os

import numpy as np

from cmimage.file_io import imread, imsave
from cmimage.preprocessing import separate_grid, random_crop, \
    detect_object_color, detect_object_mask, detect_objects_mask
from cmimage.utils.check import get_mask_rate

IMG_PATH = './datasets/images'
MASK_PATH = './datasets/labels'
OUTPUT_PATH = './datasets'

CROP_SIZE = 64


def main():
    filenames = os.listdir(IMG_PATH)

    for fn in filenames:
        header, ext = os.path.splitext(fn)
        img = imread(os.path.join(IMG_PATH, fn))
        mask = imread(os.path.join(MASK_PATH, fn),
                      rescale=1. / 255).astype(np.uint8)

        # Separates image into grid images
        crop_images, crop_points = separate_grid(img, crop_size=CROP_SIZE)
        crop_masks = [mask[y: y+CROP_SIZE, x: x+CROP_SIZE]
                      for x, y in crop_points]

        for crop_image, crop_mask, point \
                in zip(crop_images, crop_masks, crop_points):
            crop_filename = header + '__' \
                            + 'x={}_y={}'.format(point[0], point[1]) \
                            + '_size={}'.format(CROP_SIZE) \
                            + ext
            # Compute mask rate
            if get_mask_rate(crop_mask) == 0:
                imsave(os.path.join(OUTPUT_PATH, 'separate', 'x', '0',
                                    crop_filename), crop_image)
                imsave(os.path.join(OUTPUT_PATH, 'separate', 'y', '0',
                                    crop_filename), crop_mask, rescale=255)

            else:
                imsave(os.path.join(OUTPUT_PATH, 'separate', 'x', '1',
                                    crop_filename), crop_image)
                imsave(os.path.join(OUTPUT_PATH, 'separate', 'y', '1',
                                    crop_filename), crop_mask, rescale=255)

        # Apply random cropping on images as data augmentation
        crop_images, crop_points = random_crop(img, crop_size=CROP_SIZE, n=10,
                                               seed=1)
        crop_masks = [mask[y: y+CROP_SIZE, x: x+CROP_SIZE]
                      for x, y in crop_points]

        for crop_image, crop_mask, point \
                in zip(crop_images, crop_masks, crop_points):
            crop_filename = header + '__' \
                            + 'x={}_y={}'.format(point[0], point[1]) \
                            + '_size={}'.format(CROP_SIZE) \
                            + ext
            # Compute mask rate
            if get_mask_rate(crop_mask) == 0:
                imsave(os.path.join(OUTPUT_PATH, 'random', 'x', '0',
                                    crop_filename), crop_image)
                imsave(os.path.join(OUTPUT_PATH, 'random', 'y', '0',
                                    crop_filename), crop_mask, rescale=255)

            else:
                imsave(os.path.join(OUTPUT_PATH, 'random', 'x', '1',
                                    crop_filename), crop_image)
                imsave(os.path.join(OUTPUT_PATH, 'random', 'y', '1',
                                    crop_filename), crop_mask, rescale=255)

        # Detects an object within the color range as a bounding rectangle image
        color_range = ((30, 0, 30), (100, 255, 100))
        object_image, rect \
            = detect_object_color(img, 'rgb', color_range,
                                  minimum_object_rate=0.0)
        object_mask = mask[rect[1]: rect[1]+rect[3], rect[0]: rect[0]+rect[2]]
        object_filename = header + '__' \
                                 + 'x={}_y={}'.format(rect[0], rect[1]) \
                                 + '_w={}_h={}'.format(rect[2], rect[3]) \
                                 + ext
        imsave(os.path.join(OUTPUT_PATH, 'object_color', 'x', object_filename),
               object_image)
        imsave(os.path.join(OUTPUT_PATH, 'object_color', 'y', object_filename),
               object_mask, rescale=255)

        # Detects a bounding rectangle image in which all objects in mask appear
        object_image, object_mask, rect = detect_object_mask(img, mask)
        object_filename = header + '__' \
                                 + 'x={}_y={}'.format(rect[0], rect[1]) \
                                 + '_w={}_h={}'.format(rect[2], rect[3]) \
                                 + ext
        imsave(os.path.join(OUTPUT_PATH, 'object_mask', 'x', object_filename),
               object_image)
        imsave(os.path.join(OUTPUT_PATH, 'object_mask', 'y', object_filename),
               object_mask, rescale=255)


        # Detects the bounding rectangle images in which each objects in mask appear
        object_images, objects_mask, list_rect \
            = detect_objects_mask(img, mask, minimum_object_rate=0.001, margin=10)
        for object_image, object_mask, rect in zip(object_images,
                                                   objects_mask,
                                                   list_rect):
            object_filename = header + '__' \
                                     + 'x={}_y={}'.format(rect[0], rect[1]) \
                                     + '_w={}_h={}'.format(rect[2], rect[3]) \
                                     + ext
            imsave(os.path.join(OUTPUT_PATH, 'objects_mask', 'x',
                                object_filename),
                   object_image)
            imsave(os.path.join(OUTPUT_PATH, 'objects_mask', 'y',
                                object_filename),
                   object_mask, rescale=255)


if __name__ == '__main__':
    main()
