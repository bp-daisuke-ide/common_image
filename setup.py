#!/usr/bin/env python

import os
from setuptools import setup, find_packages


def read(fname):
    if os.path.exists(fname):
        return open(os.path.join(os.path.dirname(__file__), fname)).read()
    else:
        return ""

setup(
    name='common_image',
    version='0.1',
    description='Common module for projects using image',
    long_description=read('README.md'),
    url='https://bitbucket.org/bp-daisuke-ide/common_image/src/master/',
    author='daisuke.ide',
    author_email='daisuke.ide@brainpad.co.jp',
    packages=find_packages(),
    install_requires=read('requirements.txt').splitlines(),
)
